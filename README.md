## Despite giving me more time for this project I apologise  if this didn't live up to your standards.

_Throughout this project I give credit to Cameron Scholes, Tomas Smith and Aaron Richardson for helping me with my AddItemsProcess_

Initially my project was meant to be a online shopping site, the initial basket would be the shop and other baskets created would be the user baskets. I realise now in the case of this project using this as 2 tables isn't exactly efficient as it would just move the items between baskets.

__I beleived at first that it was impossible to <s>chaneg</s> change the foregin key of an object.__
I later found that this could be done by _cascading_ the object within the table.


---
<s>One of my major problems was/is making a table update from a list according to the url it does pass through as succeeding:</s>

![my success url](./assets/images/why_tho.png)


The code within the items.php page:
```

           <form action='<?= APP_BASE_URL ?>/items' action='Items.php' method='post'>
           <?php 
           
           foreach($locals['items'] as $submission) {
           ?>
            <tr>
                    <td><?= $submission['itemName'] ?> </td>
                    <td><?= $submission['itemPrice'] ?></td>
                    <td><?= $submission['basketId'] ?></</td>
                    <td>
                    <select name ='ChangeBasketId'>
                    <option  value='1'> 1  </option>
                    <option  value='2'> 2  </option>
                    </select>
                    </td>       
                    <td> <input type='submit' value='Change basket' ></td>
            </tr>  
            <?php }
            ?>
```  

__This issue was then later resolved with the help of my lecturer, this problem was seemingly caused by the form being outside of the foreach causing the change to not be sent through properly.__

---
Though I was able to put my site onlibe through 000webhost I was not able to change pages without getting an erro, this may have t do with the app base url, but I unfortunatly didn't have time to go through it before submission