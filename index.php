<?php
try {
  $config = include('config.php');
  define('APP_BASE_URL', '');

  //define ('APP_BASE_PATH', $config['app_base_url']);
  //define('BASE_URL', Config::BASE_URL);  
  
    // Include the Rapid library
    require_once('lib/Rapid.php');
  
    // Create a new Router instance
    $app = new \Rapid\Router();
  
    // Define some routes. Here: requests to / will be
    // processed by the controller at controllers/Home.php
    $app->GET('/',        'Home');
    $app->GET('/baskets', 'Baskets');
    $app->GET('/add-baskets', 'AddBaskets');
    $app->GET('/items', 'Items');
    $app->POST('/items', 'AddItemsProcess');
    $app->POST('/add-baskets', 'AddBasketProcess');
  
    // Process the request
    $app->dispatch();
  
}

catch(\Rapid\RouteNotFoundException $e){
 $res = $e ->getResponseObject();
 $res->render('main', '404', []);
}

catch(PDOException $e){
    $e -> getMessage();
}

catch(Exception $e){
  http_response_code(500);
  exit();
  //500 internal server error
}

?>