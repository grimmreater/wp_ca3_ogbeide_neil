<?php return function($req, $res){

$db = include('lib/database.php'); 


$basketName = $req->body('basketName');
$basketOwner = $req->body('basketOwner');
$basketDate = date('Y-m-d');

$add      = 'INSERT INTO baskets(basketName, basketOwner, basketDate) VALUES (:basketName, :basketOwner, :basketDate)';

$insert = $db->prepare($add);


$insert->execute([
      ':basketName'  => $basketName,
      ':basketOwner' => $basketOwner,
      ':basketDate'  => date('Y-m-d')
]);



$res->redirect("/add-baskets?success=1&$add");

}?>