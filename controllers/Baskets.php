<?php return function($req, $res) {

$db = include('lib/database.php'); 


$select      = $db->prepare('SELECT basketName, basketOwner, basketDate FROM baskets');
$select->execute();
$result= $select->fetchAll();

$res->render('main', 'baskets', [
    'basket' => $result,
    'pageTitle' => 'Basket Page'
]);

} ?>